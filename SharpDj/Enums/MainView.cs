﻿namespace SharpDj.Enums
{
    public enum MainView : ushort
    {
        Main,
        Room,
        Login,
        About,
    }

    public enum LeftBar : ushort
    {
        Visible,
        Collapsed
    }

    public enum Playlist : ushort
    {
        Visible,
        Collapsed
    }
}